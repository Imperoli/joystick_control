#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <stdio.h>
#include <boost/thread.hpp>

using namespace std;

#define PS3_BUTTON_SELECT            0
#define PS3_BUTTON_STICK_LEFT        1
#define PS3_BUTTON_STICK_RIGHT       2
#define PS3_BUTTON_START             3
#define PS3_BUTTON_CROSS_UP          4
#define PS3_BUTTON_CROSS_RIGHT       5
#define PS3_BUTTON_CROSS_DOWN        6
#define PS3_BUTTON_CROSS_LEFT        7
#define PS3_BUTTON_REAR_LEFT_2       8
#define PS3_BUTTON_REAR_RIGHT_2      9
#define PS3_BUTTON_REAR_LEFT_1       10
#define PS3_BUTTON_REAR_RIGHT_1      11
#define PS3_BUTTON_ACTION_TRIANGLE   12
#define PS3_BUTTON_ACTION_CIRCLE     13
#define PS3_BUTTON_ACTION_CROSS      14
#define PS3_BUTTON_ACTION_SQUARE     15
#define PS3_BUTTON_PAIRING           16

#define PS3_AXIS_STICK_LEFT_LEFTWARDS    0
#define PS3_AXIS_STICK_LEFT_UPWARDS      1
#define PS3_AXIS_STICK_RIGHT_LEFTWARDS   2
#define PS3_AXIS_STICK_RIGHT_UPWARDS     3
#define PS3_AXIS_BUTTON_CROSS_UP         4
#define PS3_AXIS_BUTTON_CROSS_RIGHT      5
#define PS3_AXIS_BUTTON_CROSS_DOWN       6
#define PS3_AXIS_BUTTON_CROSS_LEFT       7
#define PS3_AXIS_BUTTON_REAR_LEFT_2      8
#define PS3_AXIS_BUTTON_REAR_RIGHT_2     9
#define PS3_AXIS_BUTTON_REAR_LEFT_1      10
#define PS3_AXIS_BUTTON_REAR_RIGHT_1     11
#define PS3_AXIS_BUTTON_ACTION_TRIANGLE  12
#define PS3_AXIS_BUTTON_ACTION_CIRCLE    13
#define PS3_AXIS_BUTTON_ACTION_CROSS     14
#define PS3_AXIS_BUTTON_ACTION_SQUARE    15
#define PS3_AXIS_ACCELEROMETER_LEFT      16
#define PS3_AXIS_ACCELEROMETER_FORWARD   17
#define PS3_AXIS_ACCELEROMETER_UP        18
#define PS3_AXIS_GYRO_YAW                19

#define LOGITECH_BUTTON_BLUE 0
#define LOGITECH_BUTTON_GREEN 1
#define LOGITECH_BUTTON_RED 2
#define LOGITECH_BUTTON_YELLOW 3
#define LOGITECH_BUTTON_LB 4
#define LOGITECH_BUTTON_RB 5
#define LOGITECH_BUTTON_LT 6
#define LOGITECH_BUTTON_RT 7
#define LOGITECH_BUTTON_BACK 8
#define LOGITECH_BUTTON_START 9

sensor_msgs::Joy joy;
geometry_msgs::Twist vel;
ros::Publisher pub;

int mode=0;

double vel_scale=1;
double joy_linear_speed_scale, joy_angular_speed_scale;
boost::mutex mtx_mode, mtx_vel_scale, mtx_vel;

void vel_scaling_Callback(const geometry_msgs::Twist::ConstPtr& msg)
{
  mtx_mode.lock();
  if(mode==1)
  {
    mtx_vel.lock();
    mtx_vel_scale.lock();
    vel.linear.x=msg->linear.x*vel_scale;
    vel.linear.y=0;
    vel.angular.z=msg->angular.z*vel_scale;
    pub.publish(vel);
    mtx_vel_scale.unlock();
    mtx_vel.unlock();
  }
  mtx_mode.unlock();
}

void joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
    mtx_mode.lock();
    if(msg->buttons[LOGITECH_BUTTON_LB]==1){ mode=0;}
    else if(msg->buttons[LOGITECH_BUTTON_LT]==1){ mode=1;}
    mtx_mode.unlock();
  
    if(mode==0)
    {
        mtx_vel.lock();
        vel.linear.x=msg->axes[PS3_AXIS_STICK_LEFT_UPWARDS]*joy_linear_speed_scale;
        vel.linear.y=0;
        vel.angular.z=msg->axes[PS3_AXIS_STICK_RIGHT_LEFTWARDS]*joy_angular_speed_scale;
        pub.publish(vel);
        mtx_vel.unlock();
    }else if(mode==1)
    {
        mtx_vel_scale.lock();
        vel_scale=msg->axes[PS3_AXIS_STICK_LEFT_UPWARDS];
        if (vel_scale<0) vel_scale=0;
        mtx_vel_scale.unlock();
    }
	//cout << "Received message: " << msg->header.stamp.toSec() << " n." << cnt++ << endl;;
// 	cout << msg->axes[PS3_AXIS_BUTTON_REAR_RIGHT_2] << endl;
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "joystick_control");

    ros::NodeHandle n("~");
    std::string out_cmd_vel, in_cmd_vel, joy_topic;
    n.param("out_cmd_vel", out_cmd_vel, std::string("cmd_vel"));
    n.param("in_cmd_vel", in_cmd_vel, std::string("desired_cmd_vel"));
    n.param("joy_topic", joy_topic, std::string("joy"));
    n.param("joy_linear_speed_scale", joy_linear_speed_scale, .5);
    n.param("joy_angular_speed_scale", joy_angular_speed_scale, .5);

    pub = n.advertise<geometry_msgs::Twist>(out_cmd_vel, 1);
    ros::Subscriber vel_sub = n.subscribe<geometry_msgs::Twist>(in_cmd_vel, 1, vel_scaling_Callback);
    ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>(joy_topic, 1, joyCallback);

    ros::spin();

    return 0;
}




